{
  matra = {
    pkgs,
    config,
    ...
  }: {
    users.users = {
      server = {
      	isNormalUser = true;
        extraGroups = ["wheel"];
        initialPassword = "server";
      };
    };
    networking.dhcpcd.enable = true;
    services.openssh.enable = true;
    nixpkgs.localSystem.system = "x86_64-linux";
  };

  tatra = {
    pkgs,
    config,
    ...
  }: {
    users.users = {
      server = {
        isNormalUser = true;
        extraGroups = ["wheel"];
        initialPassword = "server";
	shell = pkgs.bash;
      };
    };
    networking = {
      dhcpcd.enable = true;
    };
    services.openssh.enable = true;
    services.httpd.enable = true;
    nixpkgs.localSystem.system = "x86_64-linux";

    # Other NixOS options
  };

  fatra = {
    pkgs,
    config,
    ...
  }: {
    users.users = {
      server = {
      	isNormalUser = true;
        extraGroups = ["wheel"];
        initialPassword = "server";
      };
    };
    networking = {
      dhcpcd.enable = true;
    };
    nixpkgs.localSystem.system = "x86_64-linux";

    # Other NixOS options
  };
}
