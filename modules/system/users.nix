{pkgs, ...}: {
  users.users.server = {
    isNormalUser = true;
    description = "NixOS server user";
    extraGroups = ["networkmanager" "wheel" "video" "libvirtd"];
    shell = pkgs.fish;
    password = "server";
  };
}
