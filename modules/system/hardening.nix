{
  pkgs,
  lib,
  ...
}: {
  security.auditd.enable = true;
  security.audit.enable = true;
  security.audit.rules = ["-a exit,always -F arch=b64 -S execve"]; # policycoreutils is for load_policy, fixfiles, setfiles, setsebool, semodile, and sestatus.
  services.fwupd.enable = true;
  security.apparmor.enable = true; # build systemd with SE Linux support so it loads policy at boot and supports file labelling
  services.logrotate.checkConfig = false;
  security.lockKernelModules = true;
  security.protectKernelImage = true;
  boot.kernelParams = [
    "slab_nomerge"
    "init_on_alloc=1"
    "init_on_free=1"
    "page_alloc.shuffle=1"
    "pti=on"
    "vsyscall=none"
    "debugfs=off"
    "oops=panic"
    "module.sig_enforce=1"
    "lockdown=confidentiality"
    "mce=0"
    "quiet"
    "loglevel=0"
    "intel_iommu=on"
    "apparmor=1"
    "security=apparmor"
  ];
  nix.allowedUsers = ["@wheel"];
  environment.defaultPackages = lib.mkForce [pkgs.foot pkgs.xterm];
  services.openssh = {
    passwordAuthentication = false;
    allowSFTP = false; # Don't set this if you need sftp
    challengeResponseAuthentication = false;
    extraConfig = ''
      AllowTcpForwarding yes
      X11Forwarding no
      AllowAgentForwarding no
      AllowStreamLocalForwarding no
      AuthenticationMethods publickey '';
  };
  boot.blacklistedKernelModules = [
    "ax25"
    "netrom"
    "rose"
    "adfs"
    "affs"
    "bfs"
    "befs"
    "cramfs"
    "efs"
    "erofs"
    "exofs"
    "freevxfs"
    "f2fs"
    "hfs"
    "hpfs"
    "jfs"
    "minix"
    "nilfs2"
    "ntfs"
    "omfs"
    "qnx4"
    "qnx6"
    "sysv"
    "ufs"
  ];
}
