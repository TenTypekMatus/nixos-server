{
  imports = [
    <disko/modules/disko.nix>
    ./sysdrive-fatra.nix
    ./sysdrive-matra.nix
    ./sysdrive-tatra.nix
  ];
}
