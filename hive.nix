{
  meta = {
    # Override to pin the Nixpkgs version (recommended). This option
    # accepts one of the following:
    # - A path to a Nixpkgs checkout
    # - The Nixpkgs lambda (e.g., import <nixpkgs>)
    # - An initialized Nixpkgs attribute set
    nixpkgs = <nixpkgs>;
  };

  defaults = {pkgs, ...}: {
    imports = [
      "${builtins.fetchTarball "https://github.com/nix-community/disko/archive/master.tar.gz"}/module.nix"
      ./modules/system/maintainance.nix
      ./modules/system/svc.nix
      ./modules/system/hardening.nix
      ./modules/system/bootloader.nix
    ];
    environment.systemPackages = with pkgs; [
      vim
      wget
      curl
    ];
    system.stateVersion = "23.11";
    time.timeZone = "Europe/Bratislava";
    deployment.targetUser = "server";
    deployment.replaceUnknownProfiles = true;
  };

  matra = {
    # HP Z400
    name,
    nodes,
    ...
  }: {
    imports = [
      ./modules/server/k8s-worker.nix
    ];
    deployment.targetHost = "192.168.1.1";
    disko.devices = {
      disk = {
        vdb = {
          device = "/dev/sda";
          type = "disk";
          content = {
            type = "gpt";
            partitions = {
              ESP = {
                end = "750M";
                type = "EF00";
                content = {
                  type = "filesystem";
                  format = "vfat";
                  mountpoint = "/boot";
                };
              };
              root = {
                name = "root";
                end = "-0";
                content = {
                  type = "filesystem";
                  format = "bcachefs";
                  mountpoint = "/";
                };
              };
            };
          };
        };
      };
    };

    networking.hostName = name;
    time.timeZone = "Europe/Bratislava";
  };
  fatra = {
    # Acer
    imports = [
      #   ./modules/server/partitions/sysdrive-fatra.nix
      ./modules/server/k8s-server.nix
    ];
    deployment.targetHost = "192.168.1.3";
    disko.devices = {
      disk = {
        vdb = {
          device = "/dev/nvme0n1";
          type = "disk";
          content = {
            type = "gpt";
            partitions = {
              ESP = {
                end = "750M";
                type = "EF00";
                content = {
                  type = "filesystem";
                  format = "vfat";
                  mountpoint = "/boot";
                };
              };
              root = {
                name = "root";
                end = "-0";
                content = {
                  type = "filesystem";
                  format = "bcachefs";
                  mountpoint = "/";
                };
              };
            };
          };
        };
      };
    };
  };
  tatra = {
    # Raspberry Pi
    imports = [
      ./modules/server/k8s-worker.nix
    ];
    deployment.targetHost = "192.168.1.2";

    disko.devices = {
      disk = {
        vdb = {
          device = "/dev/mmblck0";
          type = "disk";
          content = {
            type = "gpt";
            partitions = {
              ESP = {
                end = "750M";
                type = "EF00";
                content = {
                  type = "filesystem";
                  format = "vfat";
                  mountpoint = "/boot";
                };
              };
              root = {
                name = "root";
                end = "-0";
                content = {
                  type = "filesystem";
                  format = "bcachefs";
                  mountpoint = "/";
                };
              };
            };
          };
        };
      };
    };
  };
}
